<?php

namespace Drupal\imce_dfp\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\imce_dfp\Annotation\ImceDfp;

/**
 * Manages Imce dynamic folder path plugins.
 *
 * This class extends the DefaultPluginManager to manage plugins that handle
 * dynamic folder paths in the Imce module. It is responsible for discovering,
 * instantiating, and altering the plugins defined by the `@ImceDfp` annotation.
 */
class ImceDfpManager extends DefaultPluginManager {

  /**
   * Constructs an ImceDfpManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend instance to use for caching plugin definitions.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler used to invoke the alter hook for plugin definitions.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ImceDfp',
      $namespaces,
      $module_handler,
      ImceDfpPluginInterface::class,
      ImceDfp::class
    );
    $this->alterInfo('imce_dfp_info');
    $this->setCacheBackend($cache_backend, 'imce_dfp_info');
  }

}
