<?php

namespace Drupal\imce_dfp\Service;

/**
 * Defines an interface for Imce dynamic folder path plugins.
 *
 * This interface is implemented by plugins that provide dynamic folder paths
 * for Imce profiles. Plugins adhering to this interface are responsible for
 * generating a list of paths based on the profile ID and configuration passed
 * to them.
 */
interface ImceDfpPluginInterface {

  /**
   * Retrieves a list of dynamic folder paths for an Imce profile.
   *
   * Implementing classes should generate and return an array of folder paths
   * that are dynamically determined based on the provided profile ID and
   * configuration array. This allows the Imce module to support flexible and
   * customizable folder structures that can be altered by different plugins.
   *
   * @param string $profile_id
   *   The Imce profile ID for which the paths are being requested.
   *   This IDtypically corresponds to a specific user or role-based
   *   configuration in Imce.
   * @param array $conf
   *   The configuration array associated with the Imce profile. This array
   *   contains various settings that the plugin can use to determine the
   *   appropriate folder paths.
   *
   * @return array
   *   An array of folder paths that should be made available for the given
   *   profile ID. Each path in the array should be a string representing a
   *   valid directory path within the Imce file management system.
   */
  public function getPaths(string $profile_id, array $conf): array;

}
