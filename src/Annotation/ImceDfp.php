<?php

namespace Drupal\imce_dfp\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines an ImceDfp annotation object.
 *
 * This annotation class is used to define metadata for ImceDfp plugins.
 * Plugins annotated with `@ImceDfp` are recognized by the ImceDfpManager,
 * allowing them to provide dynamic folder paths for the Imce module.
 *
 * Plugin Namespace: Plugin\ImceDfp.
 *
 * @see \Drupal\imce_dfp\Service\ImceDfpManager
 * @see \Drupal\imce_dfp\Service\ImceDfpPluginInterface
 * @see plugin_api
 *
 * @Annotation
 */
class ImceDfp extends Plugin {

  /**
   * The ImceDfp plugin ID.
   *
   * This ID uniquely identifies the plugin within the ImceDfp system.
   * It is used by the ImceDfpManager to reference and load the correct
   * plugin instance based on the ID specified in the configuration.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the ImceDfp plugin.
   *
   * This name is used to display the plugin in the user interface and can be
   * translated into different languages. The `@Translation` annotation allows
   * this field to be internationalized.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $title;

  /**
   * The human-readable description of the ImceDfp plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

}
