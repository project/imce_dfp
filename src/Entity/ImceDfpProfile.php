<?php

namespace Drupal\imce_dfp\Entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\imce\Entity\ImceProfile;

/**
 * Defines an Imce dynamic folder path profile.
 *
 * This class extends the ImceProfile to support dynamic folder paths
 * that are generated based on plugin configurations. It processes folder paths
 * that include a specific plugin keyword and dynamically updates the paths
 * using the corresponding plugin.
 */
class ImceDfpProfile extends ImceProfile {

  /**
   * Keyword used to identify and process dynamic folder paths.
   *
   * This constant is a prefix that marks folder paths as being related to
   * a specific plugin. When encountered, the system will recognize that the
   * path needs to be processed by a plugin to generate dynamic paths.
   *
   * Example:
   * If a folder path is defined as 'dfp_plugin: my_plugin', the system
   * will interpret 'my_plugin' as the plugin ID. The corresponding plugin
   * will then be loaded to dynamically generate the appropriate paths.
   */
  const PLUGIN_KEYWORD = 'dfp_plugin: ';

  /**
   * Retrieves the configuration options for the Imce profile.
   *
   * This method overrides the parent getConf() method to process any
   * dynamic folder paths that are defined using plugins. It calls
   * dynamicPathProcessor() to handle these paths before returning the
   * configuration.
   *
   * @param string $key
   *   (optional) The Imce profile ID. Defaults to NULL.
   * @param array $default
   *   (optional) The default configuration values. Defaults to NULL.
   *
   * @return mixed
   *   The processed Imce profile configuration.
   */
  public function getConf($key = NULL, $default = NULL): mixed {
    $conf = parent::getConf($key, $default);
    return $this->dynamicPathProcessor($conf);
  }

  /**
   * Processes dynamic folder paths based on plugin configurations.
   *
   * This function iterates over the provided folder paths and checks if any
   * of them contains a plugin keyword. If found, it loads the corresponding
   * plugin, retrieves the dynamic paths, and updates the folder paths
   * configuration accordingly.
   *
   * @return mixed
   *   The modified configuration array with updated folder paths.
   */
  protected function dynamicPathProcessor(array|string|null $conf): mixed {
    if (!empty($conf['folders']) && is_array($conf['folders'])) {
      // Iterate over each folder to check for the plugin keyword.
      foreach ($conf['folders'] as $key => $folder) {
        if ($this->isPluginPath($folder['path'])) {
          // Extract the plugin ID from the folder path.
          $plugin_id = $this->extractPluginId($folder['path']);
          // If a plugin ID was found, process the plugin path.
          if ($plugin_id) {
            $this->processPluginPath($conf, $key, $plugin_id, $folder['permissions']);
          }
        }
      }
    }
    return $conf;
  }

  /**
   * Checks if the given path starts with the plugin keyword.
   *
   * @param string $path
   *   The folder path to check.
   *
   * @return bool
   *   TRUE if the path contains the plugin keyword, FALSE otherwise.
   */
  private function isPluginPath(string $path): bool {
    return str_starts_with($path, self::PLUGIN_KEYWORD);
  }

  /**
   * Extracts the plugin ID from a folder path.
   *
   * The function removes the plugin keyword from the path and trims any
   * whitespace to get the plugin ID.
   *
   * @param string $path
   *   The folder path containing the plugin keyword.
   *
   * @return string|null
   *   The extracted plugin ID, or NULL if the ID is empty.
   */
  private function extractPluginId(string $path): ?string {
    $plugin_id = trim(str_replace(self::PLUGIN_KEYWORD, '', $path));
    return !empty($plugin_id) ? $plugin_id : NULL;
  }

  /**
   * Processes the folder path corresponding to a plugin ID.
   *
   * This function removes the current folder path from the configuration and
   * replaces it with paths obtained from the plugin. It then updates the
   * configuration with these new paths.
   *
   * @param array &$conf
   *   The configuration array, passed by reference.
   * @param int $key
   *   The index of the current folder path in the configuration.
   * @param string $plugin_id
   *   The extracted plugin ID used to load the plugin instance.
   * @param mixed $permissions
   *   The permissions associated with the folder path.
   */
  private function processPluginPath(array &$conf, int $key, string $plugin_id, $permissions): void {
    /** @var \Drupal\Core\Plugin\DefaultPluginManager $imce_dfp_manager */
    $imce_dfp_manager = \Drupal::service('plugin.manager.imce_dfp.path');

    // Remove the original folder path from the configuration.
    unset($conf['folders'][$key]);

    // Load the plugin and retrieve the dynamic paths.
    try {
      /** @var \Drupal\imce_dfp\Service\ImceDfpPluginInterface $imce_dfp_plugin */
      $imce_dfp_plugin = $imce_dfp_manager->createInstance($plugin_id);
      $plugin_paths = $imce_dfp_plugin->getPaths($this->id, $conf);

      // Add the dynamically generated paths to the configuration.
      if (!empty($plugin_paths)) {
        foreach ($plugin_paths as $plugin_path) {
          $conf['folders'][] = [
            'path' => $plugin_path,
            'permissions' => $permissions,
          ];
        }
      }
    }
    catch (PluginException $e) {
      // Log a warning if there was an issue loading or processing the plugin.
      \Drupal::logger('imce_dfp')->warning($e->getMessage());
    }
  }

}
