# Imce dynamic folder path

This module adds the capability to create dynamic paths in Imce profiles by using `imce_dfp` plugins.

## Setup in Imce Configuration

1. Navigate to Imce configuration at `/admin/config/media/imce`.
2. Create or edit an Imce profile.
3. In the profile settings, specify the plugin you created in the folder path configuration. Use the format `dfp_plugin: my_plugin`, where `my_plugin` is the ID of your plugin.

## How to create a plugin

1. Create a custom module (if you don't have one).
2. Create a plugin in the folder `my_module/src/Plugin/ImceDfp`.
3. See the example plugin code below:

```php
<?php

namespace Drupal\my_module\Plugin\ImceDfp;

use Drupal\Core\Plugin\PluginBase;
use Drupal\imce_dfp\Service\ImceDfpPluginInterface;

/**
 * Example plugin implementation.
 *
 * @ImceDfp (
 *   id = "my_plugin",
 *   title = @Translation("My Plugin"),
 *   description = @Translation("An example plugin for dynamic folder paths.")
 * )
 */
class MyPlugin extends PluginBase implements ImceDfpPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getPaths($profile_id, $conf): array {
    $paths = [];
    $paths[] = 'my/new/folder/path';
    return $paths;
  }
}
```

## Dependencies

- [Imce](https://www.drupal.org/project/imce): Required for managing Imce profiles and integrating dynamic folder paths
